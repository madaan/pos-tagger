#sg
import viterbi
from numpy import *
def areTagEqual(ctag, vtag):
    AMBITAGS = { 'AJ0-AV0' , 'AJ0-VVN' , 'AJ0-VVD' , 'AJ0-NN1' , 'AJ0-VVG' , 'AVP-PRP' , 'AVQ-CJS' , 'CJS-PRP' , 'CJT-DT0' , 'CRD-PNI' , 'NN1-NP0' , 'NN1-VVB' , 'NN1-VVG' , 'NN2-VVZ' , 'VVD-VVN' , 'AV0-AJ0' , 'VVN-AJ0' , 'VVD-AJ0' , 'NN1-AJ0' , 'VVG-AJ0' , 'PRP-AVP' , 'CJS-AVQ' , 'PRP-CJS' , 'DT0-CJT' , 'PNI-CRD' , 'NP0-NN1' , 'VVB-NN1' , 'VVG-NN1' , 'VVZ-NN2' , 'VVN-VVD'}

    if(ctag == vtag):
        return True
    elif(vtag.find('-') != -1 and ctag.find('-') != -1 and  vtag.split('-')[0] == ctag.split('-')[1]):
        return True
    elif(vtag.find('-') != -1 and ctag.find('-') == 1 and vtag == ctag.split('-')[0]):
        return True
    elif(vtag.find('-') == 1 and ctag.find('-') != -1 and ctag == vtag.split('-')[0]):
        return True
    else:
        return False
def PR(trainFile, testFile, ID):

    NUMTAGS = 91
    TAGINDEX = 0
    WORDINDEX = 1
 #   TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
 #   ITAGS = {0:'N', 1:'V', 2:'A', 3:'R', 4:'O'} 
    
    TAGS = {'AJ0' : 0,  'AJC' : 1,  'AJS' : 2,  'AT0' : 3,  'AV0' : 4,  'AVP' : 5,  'AVQ' : 6,  'CJC' : 7,  'CJS' : 8,  'CJT' : 9,  'CRD' : 10,  'DPS' : 11,  'DT0' : 12,  'DTQ' : 13,  'EX0' : 14,  'ITJ' : 15,  'NN0' : 16,  'NN1' : 17,  'NN2' : 18,  'NP0' : 19,  'ORD' : 20,  'PNI' : 21,  'PNP' : 22,  'PNQ' : 23,  'PNX' : 24,  'POS' : 25,  'PRF' : 26,  'PRP' : 27,  'PUL' : 28,  'PUN' : 29,  'PUQ' : 30,  'PUR' : 31,  'TO0' : 32,  'UNC' : 33,  'VBB' : 34,  'VBD' : 35,  'VBG' : 36,  'VBI' : 37,  'VBN' : 38,  'VBZ' : 39,  'VDB' : 40,  'VDD' : 41,  'VDG' : 42,  'VDI' : 43,  'VDN' : 44,  'VDZ' : 45,  'VHB' : 46,  'VHD' : 47,  'VHG' : 48,  'VHI' : 49,  'VHN' : 50,  'VHZ' : 51,  'VM0' : 52,  'VVB' : 53,  'VVD' : 54,  'VVG' : 55,  'VVI' : 56,  'VVN' : 57,  'VVZ' : 58,  'XX0' : 59,  'ZZ0' : 60,  'AJ0-AV0' : 61,  'AJ0-VVN' : 62,  'AJ0-VVD' : 63,  'AJ0-NN1' : 64,  'AJ0-VVG' : 65,  'AVP-PRP' : 66,  'AVQ-CJS' : 67,  'CJS-PRP' : 68,  'CJT-DT0' : 69,  'CRD-PNI' : 70,  'NN1-NP0' : 71,  'NN1-VVB' : 72,  'NN1-VVG' : 73,  'NN2-VVZ' : 74,  'VVD-VVN' : 75,  'AV0-AJ0' : 76,  'VVN-AJ0' : 77,  'VVD-AJ0' : 78,  'NN1-AJ0' : 79,  'VVG-AJ0' : 80,  'PRP-AVP' : 81,  'CJS-AVQ' : 82,  'PRP-CJS' : 83,  'DT0-CJT' : 84,  'PNI-CRD' : 85,  'NP0-NN1' : 86,  'VVB-NN1' : 87,  'VVG-NN1' : 88,  'VVZ-NN2' : 89,  'VVN-VVD' : 90}

    ITAGS  = {0 : 'AJ0' , 1 : 'AJC' , 2 : 'AJS' , 3 : 'AT0' , 4 : 'AV0' , 5 : 'AVP' , 6 : 'AVQ' , 7 : 'CJC' , 8 : 'CJS' , 9 : 'CJT' , 10 : 'CRD' , 11 : 'DPS' , 12 : 'DT0' , 13 : 'DTQ' , 14 : 'EX0' , 15 : 'ITJ' , 16 : 'NN0' , 17 : 'NN1' , 18 : 'NN2' , 19 : 'NP0' , 20 : 'ORD' , 21 : 'PNI' , 22 : 'PNP' , 23 : 'PNQ' , 24 : 'PNX' , 25 : 'POS' , 26 : 'PRF' , 27 : 'PRP' , 28 : 'PUL' , 29 : 'PUN' , 30 : 'PUQ' , 31 : 'PUR' , 32 : 'TO0' , 33 : 'UNC' , 34 : 'VBB' , 35 : 'VBD' , 36 : 'VBG' , 37 : 'VBI' , 38 : 'VBN' , 39 : 'VBZ' , 40 : 'VDB' , 41 : 'VDD' , 42 : 'VDG' , 43 : 'VDI' , 44 : 'VDN' , 45 : 'VDZ' , 46 : 'VHB' , 47 : 'VHD' , 48 : 'VHG' , 49 : 'VHI' , 50 : 'VHN' , 51 : 'VHZ' , 52 : 'VM0' , 53 : 'VVB' , 54 : 'VVD' , 55 : 'VVG' , 56 : 'VVI' , 57 : 'VVN' , 58 : 'VVZ' , 59 : 'XX0' , 60 : 'ZZ0' , 61 : 'AJ0-AV0' , 62 : 'AJ0-VVN' , 63 : 'AJ0-VVD' , 64 : 'AJ0-NN1' , 65 : 'AJ0-VVG' , 66 : 'AVP-PRP' , 67 : 'AVQ-CJS' , 68 : 'CJS-PRP' , 69 : 'CJT-DT0' , 70 : 'CRD-PNI' , 71 : 'NN1-NP0' , 72 : 'NN1-VVB' , 73 : 'NN1-VVG' , 74 : 'NN2-VVZ' , 75 : 'VVD-VVN' , 76 : 'AV0-AJ0' , 77 : 'VVN-AJ0' , 78 : 'VVD-AJ0' , 79 : 'NN1-AJ0' , 80 : 'VVG-AJ0' , 81 : 'PRP-AVP' , 82 : 'CJS-AVQ' , 83 : 'PRP-CJS' , 84 : 'DT0-CJT' , 85 : 'PNI-CRD' , 86 : 'NP0-NN1' , 87 : 'VVB-NN1' , 88 : 'VVG-NN1' , 89 : 'VVZ-NN2' , 90 : 'VVN-VVD'}
    

    AMBITAGS = { 'AJ0-AV0' , 'AJ0-VVN' , 'AJ0-VVD' , 'AJ0-NN1' , 'AJ0-VVG' , 'AVP-PRP' , 'AVQ-CJS' , 'CJS-PRP' , 'CJT-DT0' , 'CRD-PNI' , 'NN1-NP0' , 'NN1-VVB' , 'NN1-VVG' , 'NN2-VVZ' , 'VVD-VVN' , 'AV0-AJ0' , 'VVN-AJ0' , 'VVD-AJ0' , 'NN1-AJ0' , 'VVG-AJ0' , 'PRP-AVP' , 'CJS-AVQ' , 'PRP-CJS' , 'DT0-CJT' , 'PNI-CRD' , 'NP0-NN1' , 'VVB-NN1' , 'VVG-NN1' , 'VVZ-NN2' , 'VVN-VVD'}

    confusionMatrix = arange(NUMTAGS * NUMTAGS, dtype = int).reshape(NUMTAGS, NUMTAGS)
    confusionMatrix.fill(0)
    tW = 0
    cW = 0
    '''
    def updateCounts(ctags, vtags):
        l = len(ctags)
        for i in range(0, l):
            tW = tW + 1
            vtag = vtags[i]
            ctag = ctags[i]
            countT[TAGS[vtag]] = countT[TAGS[vtag]] + 1
            corpusT[TAGS[ctag]] = corpusT[TAGS[ctag]] + 1
            if(vtag == ctag):
                cW = cW + 1
                correctT[TAGS[vtag]] = correctT[TAGS[vtag]] + 1
            else:
                confusionMatrix[TAGS[ctag]][TAGS[vtag]] =  confusionMatrix[TAGS[ctag]][TAGS[vtag]] + 1

    '''
    #Should be reinitialized for each training file
    #Total number of correctly labelled tags
    correctT = [1] * NUMTAGS
    #Total number of tags
    countT = [1] * NUMTAGS
    #Total number of tags in the corpus
    corpusT = [1] * NUMTAGS

    P = [0] * NUMTAGS
    R = [0] * NUMTAGS
    F = [0] * NUMTAGS

    #train viterbi using those files
    v = viterbi.viterbi(trainFile) #Train using this
    testFileH = open(testFile, 'r') 
    i = 0
    for line in testFileH:
        testFileLine = stripTags(line)
        i = i + 1
        print i
        sent = ''
        for w in testFileLine['sentence']:
            sent = sent + w.lower() + ' '

        #print len(sent)
        #print sent
        sent = sent.rstrip(' ')
        TS = v.getMaxTagState(sent)
        otags = testFileLine['tags'] #original tags

        otags[len(otags) - 1] = otags[len(otags) -1].rstrip('\n')
        if(i % 100 == 0):
            print 'Thread ID : %s, Progress : %d' % (ID, i)
        #print 'Received : ', TS
        #print 'Original : ', otags
        vtags = TS
        ctags = otags #correct tags
        l = len(ctags)
        for k in range(0, l):
            tW = tW + 1
            vtag = vtags[k]
            ctag = ctags[k]
            countT[TAGS[vtag]] = countT[TAGS[vtag]] + 1
            corpusT[TAGS[ctag]] = corpusT[TAGS[ctag]] + 1
            print vtag, ctag
            '''
            if(vtag == ctag):
                cW = cW + 1
                correctT[TAGS[vtag]] = correctT[TAGS[vtag]] + 1

            elif(vtag.find('-') != -1 and ctag.find('-') != -1 and  vtag.split('-')[0] == ctag.split('-')[1]): #ambi tag
                cW = cW + 1
                correctT[TAGS[vtag]] = correctT[TAGS[vtag]] + 1
            '''
            if(areTagEqual(ctag, vtag)):
                cW = cW + 1
                correctT[TAGS[vtag]] = correctT[TAGS[vtag]] + 1
            else:
                confusionMatrix[TAGS[ctag]][TAGS[vtag]] =  confusionMatrix[TAGS[ctag]][TAGS[vtag]] + 1

        #updateCounts(testFileLine['tags'], TS)


    overallP = 0
    overallR = 0
    overallF = 0
    for j in range(0, NUMTAGS):
        P[j] = float(correctT[j]) / countT[j]
        R[j] = float(correctT[j]) / corpusT[j]
        F[j] = float(2 * P[j] * R[j]) /(P[j] + R[j])
    overallP = float(sum(correctT)) / sum(countT)
    overallR = float(sum(correctT)) / sum(corpusT)
    overallF = float(overallP * overallR * 2) / (overallP + overallR)
    for j in range(0, NUMTAGS):
        print ITAGS[j], ':'
        print "P = %f  R = %f F = %f " % (P[j], R[j], F[j])
    print "Overall ->\n P = %f, R = %f, F = %f" %(overallP, overallR, overallF)
    print 'Accuracy = %f' % (float(cW) / tW)
#P value for noun = (#Words correctly labelled as Nouns  / #Words labelled as Nouns)
#R value for noun = (#Words correctly tagged as Nouns / #Words labelled as Nouns in the corpus)
def stripTags(tagged):
    TAGINDEX = 0
    WORDINDEX = 1
    tags = []
    sentence = []
    for ele in tagged.split(' '):
        temp = ele.split('_')
        if(len(temp) == 2):
            tags.append(temp[TAGINDEX])
            sentence.append(temp[WORDINDEX])

    return {'sentence' : sentence, 'tags' : tags}


import sys
from multiprocessing import Process
if __name__ == '__main__':
    trainFiles = ['data/fold1/train.txt', 'data/fold2/train.txt', 'data/fold3/train.txt', 'data/fold4/train.txt', 'data/fold5/train.txt']
    #testFiles =  ['data/fold1/t1.txt', 'data/fold2/t2.txt',  'data/fold3/t3.txt',  'data/fold4/t4.txt',  'data/fold5/t5.txt']
    testFiles =  ['data/fold1/st.txt', 'data/fold2/test.txt',  'data/fold3/test.txt',  'data/fold4/test.txt',  'data/fold5/test.txt']
    fold1 =  Process(target = PR, args = (trainFiles[0], testFiles[0], 'fold1_thread',))
#    fold2 =  Process(target = PR, args = (trainFiles[1], testFiles[1], 'fold2_thread',))
#    fold3 =  Process(target = PR, args = (trainFiles[2], testFiles[2], 'fold3_thread',))
#    fold4 =  Process(target = PR, args = (trainFiles[3], testFiles[3], 'fold4_thread',))
#    fold5 =  Process(target = PR, args = (trainFiles[4], testFiles[4], 'fold5_thread',))
    fold1.start()
#    fold2.start()
#    fold3.start()
#    fold4.start()
#    fold5.start()

    fold1.join()
    
#    fold2.join()
#    fold3.join()
#    fold4.join()
#    fold5.join()

