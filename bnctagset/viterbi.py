#sg
#The viterbi algorithm

from numpy import *
import learnprobs
from collections import defaultdict
class viterbi:
    trainFileName = ''
    #initialProbs = arange(NUMTAGS, dtype = float)
    #transitionProbs = arange(2NUMTAGS, dtype=float).reshape(NUMTAGS,NUMTAGS)
    #emissionProbDict = defaultdict(dict)
    
    def __init__(self, trainFileName):
        self.trainFileName =  trainFileName
        #learn the probabilities
        results = learnprobs.learnProbs(trainFileName)
        self.initialProbs = results['initial']
        self.emissionProbDict = results['emission']
        self.transitionProbs = results['transition']
        self.NUMTAGS = results['NUMTAGS'] 

    #returns the most probable state sequence
    #Given the sentence
    def getMaxTagState(self, S):
        NUMTAGS = self.NUMTAGS
        '''temp is an array that will calculate the 
        temporary probabilities of the next stage
        Row -> Previous state
        Column -> Next state
        Thus, just one column remains
        '''
        temp = arange(NUMTAGS * NUMTAGS, dtype = float).reshape(NUMTAGS,NUMTAGS)

        #dictionary to supply the tag number
        #TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
        #ITAGS = {0:'N', 1:'V', 2:'A', 3:'R', 4:'O'} 
        TAGS = {'AJ0' : 0,  'AJC' : 1,  'AJS' : 2,  'AT0' : 3,  'AV0' : 4,  'AVP' : 5,  'AVQ' : 6,  'CJC' : 7,  'CJS' : 8,  'CJT' : 9,  'CRD' : 10,  'DPS' : 11,  'DT0' : 12,  'DTQ' : 13,  'EX0' : 14,  'ITJ' : 15,  'NN0' : 16,  'NN1' : 17,  'NN2' : 18,  'NP0' : 19,  'ORD' : 20,  'PNI' : 21,  'PNP' : 22,  'PNQ' : 23,  'PNX' : 24,  'POS' : 25,  'PRF' : 26,  'PRP' : 27,  'PUL' : 28,  'PUN' : 29,  'PUQ' : 30,  'PUR' : 31,  'TO0' : 32,  'UNC' : 33,  'VBB' : 34,  'VBD' : 35,  'VBG' : 36,  'VBI' : 37,  'VBN' : 38,  'VBZ' : 39,  'VDB' : 40,  'VDD' : 41,  'VDG' : 42,  'VDI' : 43,  'VDN' : 44,  'VDZ' : 45,  'VHB' : 46,  'VHD' : 47,  'VHG' : 48,  'VHI' : 49,  'VHN' : 50,  'VHZ' : 51,  'VM0' : 52,  'VVB' : 53,  'VVD' : 54,  'VVG' : 55,  'VVI' : 56,  'VVN' : 57,  'VVZ' : 58,  'XX0' : 59,  'ZZ0' : 60,  'AJ0-AV0' : 61,  'AJ0-VVN' : 62,  'AJ0-VVD' : 63,  'AJ0-NN1' : 64,  'AJ0-VVG' : 65,  'AVP-PRP' : 66,  'AVQ-CJS' : 67,  'CJS-PRP' : 68,  'CJT-DT0' : 69,  'CRD-PNI' : 70,  'NN1-NP0' : 71,  'NN1-VVB' : 72,  'NN1-VVG' : 73,  'NN2-VVZ' : 74,  'VVD-VVN' : 75,  'AV0-AJ0' : 76,  'VVN-AJ0' : 77,  'VVD-AJ0' : 78,  'NN1-AJ0' : 79,  'VVG-AJ0' : 80,  'PRP-AVP' : 81,  'CJS-AVQ' : 82,  'PRP-CJS' : 83,  'DT0-CJT' : 84,  'PNI-CRD' : 85,  'NP0-NN1' : 86,  'VVB-NN1' : 87,  'VVG-NN1' : 88,  'VVZ-NN2' : 89,  'VVN-VVD' : 90}

        ITAGS  = {0 : 'AJ0' , 1 : 'AJC' , 2 : 'AJS' , 3 : 'AT0' , 4 : 'AV0' , 5 : 'AVP' , 6 : 'AVQ' , 7 : 'CJC' , 8 : 'CJS' , 9 : 'CJT' , 10 : 'CRD' , 11 : 'DPS' , 12 : 'DT0' , 13 : 'DTQ' , 14 : 'EX0' , 15 : 'ITJ' , 16 : 'NN0' , 17 : 'NN1' , 18 : 'NN2' , 19 : 'NP0' , 20 : 'ORD' , 21 : 'PNI' , 22 : 'PNP' , 23 : 'PNQ' , 24 : 'PNX' , 25 : 'POS' , 26 : 'PRF' , 27 : 'PRP' , 28 : 'PUL' , 29 : 'PUN' , 30 : 'PUQ' , 31 : 'PUR' , 32 : 'TO0' , 33 : 'UNC' , 34 : 'VBB' , 35 : 'VBD' , 36 : 'VBG' , 37 : 'VBI' , 38 : 'VBN' , 39 : 'VBZ' , 40 : 'VDB' , 41 : 'VDD' , 42 : 'VDG' , 43 : 'VDI' , 44 : 'VDN' , 45 : 'VDZ' , 46 : 'VHB' , 47 : 'VHD' , 48 : 'VHG' , 49 : 'VHI' , 50 : 'VHN' , 51 : 'VHZ' , 52 : 'VM0' , 53 : 'VVB' , 54 : 'VVD' , 55 : 'VVG' , 56 : 'VVI' , 57 : 'VVN' , 58 : 'VVZ' , 59 : 'XX0' , 60 : 'ZZ0' , 61 : 'AJ0-AV0' , 62 : 'AJ0-VVN' , 63 : 'AJ0-VVD' , 64 : 'AJ0-NN1' , 65 : 'AJ0-VVG' , 66 : 'AVP-PRP' , 67 : 'AVQ-CJS' , 68 : 'CJS-PRP' , 69 : 'CJT-DT0' , 70 : 'CRD-PNI' , 71 : 'NN1-NP0' , 72 : 'NN1-VVB' , 73 : 'NN1-VVG' , 74 : 'NN2-VVZ' , 75 : 'VVD-VVN' , 76 : 'AV0-AJ0' , 77 : 'VVN-AJ0' , 78 : 'VVD-AJ0' , 79 : 'NN1-AJ0' , 80 : 'VVG-AJ0' , 81 : 'PRP-AVP' , 82 : 'CJS-AVQ' , 83 : 'PRP-CJS' , 84 : 'DT0-CJT' , 85 : 'PNI-CRD' , 86 : 'NP0-NN1' , 87 : 'VVB-NN1' , 88 : 'VVG-NN1' , 89 : 'VVZ-NN2' , 90 : 'VVN-VVD'}

        '''
        p is the cumulative probability at each level. 
        Assuming max length SLEN_SENTENCE_LENGTH
        '''
        SLEN_SENTENCE_LENGTH = 500
        p = arange(SLEN_SENTENCE_LENGTH * NUMTAGS, dtype = float).reshape(SLEN_SENTENCE_LENGTH, NUMTAGS)

        p.fill(0)

        '''
        Parent is the parent of each tag at each level
        '''
        parent = arange(SLEN_SENTENCE_LENGTH * NUMTAGS, dtype = int).reshape(SLEN_SENTENCE_LENGTH,NUMTAGS)
        parent.fill(-1)
        #initilize init
        for tagIndex in ITAGS:
            p[0][tagIndex] = self.initialProbs[tagIndex]


        #words = S
        words = S.split(' ')
        SLEN = len(words)
        temp.fill(0)

        ##################
        #NOTE ON INDEXES
        #1. The single node level is not counted at all.
        #2. The first 5 node level is called level 0, and so on.
        ###################

        ##############
        #TRELLES CONSTRUCTION BEGINS
        ##############

        for level in range(1, SLEN):

            ### Begin creating the next level by expanding the previous one
            for tagU in TAGS: #The upper level
                ui = TAGS[tagU] #just the tag number of the tag which is being expanded(the upper level)
                if(words[level - 1].lower() in self.emissionProbDict[tagU]):
                    ep = self.emissionProbDict[tagU][words[level - 1]] #emission probability
                else:
                    ### 
                    #SMOOTHING : ASSIGN BARE MINIMUM PROBABILITY TO THE MISSING
                    ###
                    ep = 0.000001
                for tagL in TAGS:
                    li = TAGS[tagL]
                    temp[ui, li] =  p[level - 1, ui] * self.transitionProbs[ui][li] * ep
                    #temp[ui][li] = 0.01

             #### 
             #FINISH EXPANSION OF THE CURRENT LEVEL
             ####


            ###
            #PRUNE THE EXPANDED NEXT LEVEL TO RETAIN JUST THE SLEN OF EACH TAG
            # RETAIN THE MAX IN EACH COLUMN OF TEMP
            ###

            maxVal = [None] * NUMTAGS
            maxIndex = [None] * NUMTAGS
            for i in range(0, NUMTAGS):
                maxVal[i] = max(temp[:,i])
                maxIndex[i] = temp[:,i].argmax() #our old friend
                p[level][i] = maxVal[i]
                parent[level][i] = maxIndex[i]
        
        #Done with all the levels but the last
        #which should not be fully expanded

        
        for i in range(0, NUMTAGS):
            if(words[SLEN - 1].lower() in self.emissionProbDict[ITAGS[i]]):
                ep = self.emissionProbDict[ITAGS[i]][words[SLEN - 1]] #emission probability
            else:
                ep = 0
            p[SLEN][i] = p[SLEN - 1][i] * ep
            parent[SLEN][i] = i

        #FIND OUT MAX AT THE LAST LEVEL, SO THAT YOU GET A TIP TO HOLD ON TO
        #AND RIDE TO THE TOP
        maxTagV = p[SLEN][0]
        maxTag = 0
        TS = [None] * SLEN

        maxTagV = max(p[SLEN])
        maxTag = p[SLEN].argmax()
        #the tag for the last word
        TS[SLEN - 1] = ITAGS[29]
        
        #parent of the last word
        parentT = parent[SLEN][maxTag]
        
        for level in range(SLEN - 1, 0, -1):
            parentT = parent[level][parentT]
            TS[level - 1] = ITAGS[parentT]
       #TS : Tag sequence -> indexed by level number - 1
        return TS

if __name__ == '__main__':

    print 'Training the tagger, please wait'
    v = viterbi('data/fold1/train.txt')
    while(True):
        x = input('Enter your sentence wrapped in single quotes : ')
        print v.getMaxTagState(x)
