#sg

from numpy import *
'''
Just returns the correct index into the transitionProbs

'''
'''
The Module to learn the translation and emission probabilities
'''
def learnProbs(fileName):
    NUMTAGS = 91
    TAGINDEX = 0
    WORDINDEX = 1
    transitionProbs = arange(NUMTAGS * NUMTAGS, dtype=float).reshape(NUMTAGS, NUMTAGS)
    transitionProbs.fill(0)
    '''
       N V A R O
    N
    V
    A
    R
    O
    '''
    initialProbs = arange(NUMTAGS, dtype = float)

    taggedCorpus = open(fileName)
    #TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
    TAGS = {'AJ0' : 0,  'AJC' : 1,  'AJS' : 2,  'AT0' : 3,  'AV0' : 4,  'AVP' : 5,  'AVQ' : 6,  'CJC' : 7,  'CJS' : 8,  'CJT' : 9,  'CRD' : 10,  'DPS' : 11,  'DT0' : 12,  'DTQ' : 13,  'EX0' : 14,  'ITJ' : 15,  'NN0' : 16,  'NN1' : 17,  'NN2' : 18,  'NP0' : 19,  'ORD' : 20,  'PNI' : 21,  'PNP' : 22,  'PNQ' : 23,  'PNX' : 24,  'POS' : 25,  'PRF' : 26,  'PRP' : 27,  'PUL' : 28,  'PUN' : 29,  'PUQ' : 30,  'PUR' : 31,  'TO0' : 32,  'UNC' : 33,  'VBB' : 34,  'VBD' : 35,  'VBG' : 36,  'VBI' : 37,  'VBN' : 38,  'VBZ' : 39,  'VDB' : 40,  'VDD' : 41,  'VDG' : 42,  'VDI' : 43,  'VDN' : 44,  'VDZ' : 45,  'VHB' : 46,  'VHD' : 47,  'VHG' : 48,  'VHI' : 49,  'VHN' : 50,  'VHZ' : 51,  'VM0' : 52,  'VVB' : 53,  'VVD' : 54,  'VVG' : 55,  'VVI' : 56,  'VVN' : 57,  'VVZ' : 58,  'XX0' : 59,  'ZZ0' : 60,  'AJ0-AV0' : 61,  'AJ0-VVN' : 62,  'AJ0-VVD' : 63,  'AJ0-NN1' : 64,  'AJ0-VVG' : 65,  'AVP-PRP' : 66,  'AVQ-CJS' : 67,  'CJS-PRP' : 68,  'CJT-DT0' : 69,  'CRD-PNI' : 70,  'NN1-NP0' : 71,  'NN1-VVB' : 72,  'NN1-VVG' : 73,  'NN2-VVZ' : 74,  'VVD-VVN' : 75,  'AV0-AJ0' : 76,  'VVN-AJ0' : 77,  'VVD-AJ0' : 78,  'NN1-AJ0' : 79,  'VVG-AJ0' : 80,  'PRP-AVP' : 81,  'CJS-AVQ' : 82,  'PRP-CJS' : 83,  'DT0-CJT' : 84,  'PNI-CRD' : 85,  'NP0-NN1' : 86,  'VVB-NN1' : 87,  'VVG-NN1' : 88,  'VVZ-NN2' : 89,  'VVN-VVD' : 90}
    from collections import defaultdict
    tagWordList = defaultdict(list)

    for line in taggedCorpus: #for each line in corpus
        #print line
        words = line.rstrip('\n').split(' ')
        prevTag = 'X' #not yet started
        firstTag = words[0].split('_')[TAGINDEX]
        initialProbs[TAGS[firstTag]] = initialProbs[TAGS[firstTag]] + 1
        for w in words:
            #CONVERT WORD TO LOWER
            word = w.split('_')[WORDINDEX].lower()
            tag = w.split('_')[TAGINDEX]
            if(tag == ''):
                continue
            if(prevTag != 'X'):
                pt = TAGS[prevTag]
                ct = TAGS[tag]
                transitionProbs[pt, ct] = transitionProbs[pt, ct] + 1
            
            prevTag = tag

            #Calculating the counts :
            '''For each tag, create a list in which to store the word belonging to that tag.
            also maintain the transition probabilities for the last match'''
            tagWordList[tag].append(word)

    for i in range(0, NUMTAGS):
        rowSum = 1
        for j in range(0, NUMTAGS):
            rowSum = rowSum + transitionProbs[i][j] 
        for j in range(0, NUMTAGS):
            transitionProbs[i][j] = float(transitionProbs[i][j]) / rowSum
    
    from collections import Counter
    

    emissionProbDict = defaultdict(dict)
    #Emission probability is a dict, first indexed by Tag, then by word
    for tag in tagWordList:
        emissionProbDict[tag] = Counter(tagWordList[tag])
    

    #initial probabiities
    isum = sum(initialProbs, dtype = float)
    for i in range(0, NUMTAGS):
        initialProbs[i] = initialProbs[i] / isum

    #print emissionProbDict['R']
    #print emissionProbDict['V']['keep']
    for key in emissionProbDict.keys(): #for all tags
        total = sum(emissionProbDict[key].values())
        for key1 in emissionProbDict[key].keys():
            emissionProbDict[key][key1] = float(emissionProbDict[key][key1]) / total

    print 'Training finished, returning tables...'
    #savetxt('initialProbs', initialProbs, delimiter = ',')
    #savetxt('emissionProbs', emissionProbDict, delimiter = ',')
    savetxt('transitionProbs', transitionProbs, delimiter = ',')
    return {'initial' : initialProbs, 'emission' : emissionProbDict, 'transition' : transitionProbs, 'NUMTAGS' : NUMTAGS}

if __name__ == "__main__":
    learnProbs('data/train.txt')

