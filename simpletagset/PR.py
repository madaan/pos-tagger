#sg
import viterbi
from numpy import *
def PR():
    trainFiles = ['data/train1.txt', 'data/train2.txt', 'data/train3.txt', 'data/train4.txt', 'data/train5.txt'] 
    testFiles = ['data/test1.txt', 'data/test2.txt', 'data/test3.txt', 'data/test4.txt', 'data/test5.txt']

    NUMTAGS = 5
    TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
    ITAGS = {0:'N', 1:'V', 2:'A', 3:'R', 4:'O'} 
    
    confusionMatrix = arange(NUMTAGS * NUMTAGS, dtype = int).reshape(NUMTAGS, NUMTAGS)
    confusionMatrix.fill(0)

    NUMRUNS = 5
    
    avP = arange(NUMRUNS * NUMTAGS, dtype = float).reshape(NUMRUNS, NUMTAGS)
    avR = arange(NUMRUNS * NUMTAGS, dtype = float).reshape(NUMRUNS, NUMTAGS)
    avF = arange(NUMRUNS * NUMTAGS, dtype = float).reshape(NUMRUNS, NUMTAGS)
    
    for run in range(0, NUMRUNS):
        print 'Run : ', run + 1
        #Should be reinitialized for each training file
        #Total number of correctly labelled tags
        correctT = [0] * NUMTAGS
        #Total number of tags
        countT = [0] * NUMTAGS
        #Total number of tags in the corpus
        corpusT = [0] * NUMTAGS
    
        P = [0] * NUMTAGS
        R = [0] * NUMTAGS
        F = [0] * NUMTAGS

        def updateCounts(ctags, vtags):
            l = len(ctags)
            for i in range(0, l):
                vtag = vtags[i]
                ctag = ctags[i]
                countT[TAGS[vtag]] = countT[TAGS[vtag]] + 1
                corpusT[TAGS[ctag]] = corpusT[TAGS[ctag]] + 1
                if(vtag == ctag):
                    correctT[TAGS[vtag]] = correctT[TAGS[vtag]] + 1
                else:
                    confusionMatrix[TAGS[ctag]][TAGS[vtag]] =  confusionMatrix[TAGS[ctag]][TAGS[vtag]] + 1

#train viterbi using those files
        v = viterbi.viterbi(trainFiles[run]) #Train using this
        testFile = open(testFiles[run], 'r') 
        for line in testFile:
            testFileLine = stripTags(line)
            sent = ''
            for w in testFileLine['sentence']:
                sent = sent + w.lower() + ' '

            #print len(sent)
            #print sent
            sent = sent.rstrip(' ')
            TS = v.getMaxTagState(sent)
            otags = testFileLine['tags']
            #Calculate PR values

            otags[len(otags) - 1] = otags[len(otags) -1].rstrip('\n')
            #print 'Received : ', TS
            #print 'Original : ', otags
            updateCounts(testFileLine['tags'], TS)


        overallP = 0
        overallR = 0
        overallF = 0
        for j in range(0, NUMTAGS):
            P[j] = float(correctT[j]) / countT[j]
            R[j] = float(correctT[j]) / corpusT[j]
            F[j] = float(2 * P[j] * R[j]) /(P[j] + R[j])
        
        for j in range(0, NUMTAGS):
            avP[run][j] = P[j]
            avR[run][j] = R[j]
            avF[run][j] = F[j]
        #print 'correct',correctT,'corpus', corpusT,'count', countT
        #print sum(correctT), sum(countT), sum(corpusT)
        overallP = float(sum(correctT)) / sum(countT)
        overallR = float(sum(correctT)) / sum(corpusT)
        overallF = float(overallP * overallR * 2) / (overallP + overallR)
        #print overallP
        #print overallR
        #print overallF
        for j in range(0, NUMTAGS):
            print ITAGS[j], ':'
            print "P = %f  R = %f F = %f " % (P[j], R[j], F[j])
        print "Overall ->\n P = %f, R = %f, F = %f" %(overallP, overallR, overallF)
    
    sumP = [0] * NUMTAGS
    sumR = [0] * NUMTAGS
    sumF = [0] * NUMTAGS
    
    for tags in range(0, NUMTAGS):
        for i in range(0, NUMRUNS):
            sumP[tags] = sumP[tags]  + avP[i][tags]
            sumR[tags] = sumR[tags]  + avR[i][tags]
            sumF[tags] = sumF[tags]  + avF[i][tags]
    
    sumP = [i / NUMRUNS for i in sumP]
    sumR = [i / NUMRUNS for i in sumR]
    sumF = [i / NUMRUNS for i in sumF]
    print sumP, sumR, sumF

    print '\nConfusion Matrix -> \n' 
    print
    print '       N      V       A      R     O'
    for i in range(0, NUMTAGS):
        print ITAGS[i], 
        for j in range(0, NUMTAGS):
            print ' %5d' % confusionMatrix[i][j],
        print 


#P value for noun = (#Words correctly labelled as Nouns  / #Words labelled as Nouns)
#R value for noun = (#Words correctly tagged as Nouns / #Words labelled as Nouns in the corpus)
def stripTags(tagged):
    tags = []
    sentence = []
    for ele in tagged.split(' '):
        temp = ele.split('_')
        if(len(temp) == 2):
            tags.append(temp[1])
            sentence.append(temp[0])

    return {'sentence' : sentence, 'tags' : tags}

if __name__ == '__main__':
    PR()
