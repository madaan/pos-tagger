
from numpy import *
import learnprobs
from collections import defaultdict

class ptables:

    trainFileName = ''
    #initialProbs = arange(NUMTAGS, dtype = float)
    #transitionProbs = arange(2NUMTAGS, dtype=float).reshape(NUMTAGS,NUMTAGS)
    #emissionProbDict = defaultdict(dict)
    
    def __init__(self, trainFileName):
        self.trainFileName =  trainFileName
        #learn the probabilities
        results = learnprobs.learnProbs(trainFileName)
        self.initialProbs = results['initial']
        self.emissionProbDict = results['emission']
        self.transitionProbs = results['transition']
        print self.initialProbs

    def pget(self):

        TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
        ITAGS = {0:'N', 1:'V', 2:'A', 3:'R', 4:'O'} 
        
        while(True):
        
            x = raw_input("1. Transition\n2.Emission\n>")
            
            if(x == '1'):
                s1 = raw_input("First Stage : ")
                s2 = raw_input("Next Stage : ")
                print 'P[%s|%s] = %f' % (s2, s1, self.transitionProbs[TAGS[s1]][TAGS[s2]])
            elif(x == '2'):
                tag = raw_input("Enter Tag : ")
                word = raw_input("Enter word : ")
                print 'P[%s|%s] = %f' %(word, tag, self.emissionProbDict[tag][word])
            else :
                print "Wrong option"

            
if __name__ == "__main__":
    x  = ptables('data/ptc.txt')
    x.pget()
