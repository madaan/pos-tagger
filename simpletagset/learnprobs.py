#sg

from numpy import *
'''
Just returns the correct index into the transitionProbs

'''
'''
The Module to learn the translation and emission probabilities
'''
def learnProbs(fileName):
    transitionProbs = arange(25, dtype=float).reshape(5,5)
    transitionProbs.fill(0)
    '''
       N V A R O
    N
    V
    A
    R
    O
    '''
    initialProbs = arange(5, dtype = float)

    taggedCorpus = open(fileName)
    TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
    from collections import defaultdict
    tagWordList = defaultdict(list)

    for line in taggedCorpus: #for each line in corpus
        words = line.rstrip('\n').split(' ')
        prevTag = 'X' #not yet started
        firstTag = words[0].split('_')[1]
        initialProbs[TAGS[firstTag]] = initialProbs[TAGS[firstTag]] + 1
        for w in words:
            word = w.split('_')[0].lower()
            tag = w.split('_')[1]
            if(prevTag != 'X'):
                pt = TAGS[prevTag]
                ct = TAGS[tag]
                transitionProbs[pt][ct] = transitionProbs[pt][ct] + 1
            
            prevTag = tag

            #Calculating the counts :
            '''For each tag, create a list in which to store the word belonging to that tag.
            also maintain the transition probabilities for the last match'''
            tagWordList[tag].append(word)

    for i in range(0,5):
        rowSum = 0
        for j in range(0,5):
            rowSum = rowSum + transitionProbs[i][j] 
        for j in range(0,5):
            transitionProbs[i][j] = transitionProbs[i][j] / rowSum
    
    from collections import Counter
    

    emissionProbDict = defaultdict(dict)
    #Emission probability is a dict, first indexed by Tag, then by word
    for tag in tagWordList:
        emissionProbDict[tag] = Counter(tagWordList[tag])
    

    #initial probabiities
    isum = sum(initialProbs, dtype = float)
    for i in range(0,5):
        initialProbs[i] = initialProbs[i] / isum

    #print emissionProbDict['R']
    #print emissionProbDict['V']['keep']
    for key in emissionProbDict.keys(): #for all tags
        total = sum(emissionProbDict[key].values())
        for key1 in emissionProbDict[key].keys():
            emissionProbDict[key][key1] = float(emissionProbDict[key][key1]) / total

    return {'initial' : initialProbs, 'emission' : emissionProbDict, 'transition' : transitionProbs}

if __name__ == "__main__":
    learnProbs('data/ptc.txt')

