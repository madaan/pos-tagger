#sg
#The viterbi algorithm

from numpy import *
import learnprobs
from collections import defaultdict
class viterbi:
    trainFileName = ''
    #initialProbs = arange(NUMTAGS, dtype = float)
    #transitionProbs = arange(2NUMTAGS, dtype=float).reshape(NUMTAGS,NUMTAGS)
    #emissionProbDict = defaultdict(dict)
    
    def __init__(self, trainFileName):
        self.trainFileName =  trainFileName
        #learn the probabilities
        results = learnprobs.learnProbs(trainFileName)
        self.initialProbs = results['initial']
        self.emissionProbDict = results['emission']
        self.transitionProbs = results['transition']
    
    #returns the most probable state sequence
    #Given the sentence
    def getMaxTagState(self, S):
        NUMTAGS = 5
        '''temp is an array that will calculate the 
        temporary probabilities of the next stage
        Row -> Previous state
        Column -> Next state
        Thus, just one column remains
        '''
        temp = arange(25, dtype = float).reshape(NUMTAGS,NUMTAGS)

        #dictionary to supply the tag number
        TAGS = {'N':0, 'V':1, 'A':2, 'R':3, 'O':4}
        ITAGS = {0:'N', 1:'V', 2:'A', 3:'R', 4:'O'} 

        '''
        p is the cumulative probability at each level. 
        Assuming max length 20
        '''
        p = arange(500, dtype = float).reshape(100,NUMTAGS)

        p.fill(0)

        '''
        Parent is the parent of each tag at each level
        '''
        parent = arange(500, dtype = int).reshape(100,NUMTAGS)
        parent.fill(-1)
        #initilize init
        p[0][TAGS['N']] = self.initialProbs[TAGS['N']]
        p[0][TAGS['V']] = self.initialProbs[TAGS['V']]
        p[0][TAGS['A']] = self.initialProbs[TAGS['A']]
        p[0][TAGS['R']] = self.initialProbs[TAGS['R']]
        p[0][TAGS['O']] = self.initialProbs[TAGS['O']]
        

        #words = S
        words = S.split(' ')
        MAX = len(words)
        temp.fill(0)
        for level in range(1, MAX):
            #print "processing word", words[level - 1]

            for tagU in TAGS: #The upper level
                ui = TAGS[tagU] #just the tag number of the tag which is being expanded(the upper level)
                if(words[level - 1].lower() in self.emissionProbDict[tagU]):
                    ep = self.emissionProbDict[tagU][words[level - 1]] #emission probability
                else:
                    ep = 0.0001
                for tagL in TAGS:
                    li = TAGS[tagL]
                    temp[ui][li] =  p[level - 1][ui] * self.transitionProbs[ui][li] * ep
             
             #finished expanding the current level, learn the proabbilities of the next one
             #Only one of each tag should remain

            maxVal = [None] * NUMTAGS
            maxIndex = [None] * NUMTAGS
            for i in range(0, NUMTAGS):
                maxVal[i] = temp[0][i]
                maxIndex[i] = 0
                for j in range(1, NUMTAGS):
                    if(temp[j][i] > maxVal[i]):
                        maxVal[i] = temp[j][i]
                        maxIndex[i] = j

            for i in range(0, NUMTAGS):
                p[level][i] = maxVal[i]
                parent[level][i] = maxIndex[i]
        
        #Done with all the levels but the last
        #which should not be fully expanded

        
        for i in range(0, NUMTAGS):
            if(words[MAX - 1].lower() in self.emissionProbDict[ITAGS[i]]):
                ep = self.emissionProbDict[ITAGS[i]][words[MAX - 1]] #emission probability
            else:
                ep = 0
            p[MAX][i] = p[MAX - 1][i] * ep
            parent[MAX][i] = i
        #FIND OUT MAX AT LAST LEVEL, SO THAT YOU GET A TIP TO HOLD ON TO
        #AND RIDE TO THE TOP
        maxTagV = p[MAX][0]
        maxTag = 0
        TS = [None] * MAX
        for j in range(1,NUMTAGS):
            if(p[MAX][j] > maxTagV):
                maxTagV= p[MAX][j]
                maxTag = j

        #the tag for the last word
        TS[MAX - 1] = ITAGS[maxTag]
        
        #parent of the last word
        parentT = parent[MAX][maxTag]
        
        for level in range(MAX - 1, 0, -1):
            parentT = parent[level][parentT]
            TS[level - 1] = ITAGS[parentT]
       #TS : Tag sequence -> indexed by level number - 1
        return TS

if __name__ == '__main__':

    v = viterbi('data/ptc.txt')
    while(True):
        x = input('Enter your sentence wrapped in single quotes : ')
        S = v.getMaxTagState(x)
        l = len(S)
        ip = x.split(' ')
        for i in range(0, l):
            print "%s_%s" % (ip[i], S[i]),
        print 
