##
state <-> tag throughout
##

At each level, the following DS will be maintained : 
1. P[level, tag] : float
    Cumulative probability of reaching a state
at that level. Note that for each level, only *one* P value will 
be defined for each state, the minimum.

2. Parent[level, tag] : character
    Parent tag of each tag in the current level.

Approach :

First expand the next level, then calculate the minimums. Record the parent
of the minimums.

When the levels end(the sentence gets over), pick the highest P Value at that state, print the tag of that P value, go to it's parent and repeat.


Reference : Took help from wiki page of the Viterbi algorithm


##DOUBT 
How is the last stage treated?


